<?php

class Konto
{
private $numer;
function __construct($s)
{
$this->numer = $s;
}
public function setNumer($wartosc)
{
$this->numer = $wartosc;
}
public function getNumer()
{
return $this->numer;
}
}

class Klient extends Konto
{
private $ime;
private $nazwisko;
function __construct($i,$n)
{
$this->imie = $i;
$this->nazwisko = $n;
}
public function setImie($wartosc)
{
$this->imie = $wartosc;
}
public function getImie()
{
return $this->imie;
}
public function setNazwisko($wartosc)
{
$this->nazwisko = $wartosc;
}
public function getNazwisko()
{
return $this->nazwisko;
}
}
$konto1 = new Klient('Bartosz','Krawczyk');
$konto1->setNumer('63 123 2137 123456 22');
echo 'Numer konta: '.$konto1->getNumer()."</br>";
echo "<br>";
echo 'Imię właściciela: '.$konto1->getImie()."<br>";
echo 'Nazwisko właściciela: '.$konto1->getNazwisko()."<br>";
?>
