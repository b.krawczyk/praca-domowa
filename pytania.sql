-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 10 Sty 2022, 08:27
-- Wersja serwera: 10.4.21-MariaDB
-- Wersja PHP: 8.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `3ti`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pytania`
--

CREATE TABLE `pytania` (
  `id` int(10) NOT NULL,
  `pytanie` text COLLATE utf8_polish_ci NOT NULL,
  `odp a` varchar(250) COLLATE utf8_polish_ci NOT NULL,
  `odp b` varchar(250) COLLATE utf8_polish_ci NOT NULL,
  `odp c` varchar(250) COLLATE utf8_polish_ci NOT NULL,
  `odp d` varchar(250) COLLATE utf8_polish_ci NOT NULL,
  `prawidlowa odpowiedz` varchar(250) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `pytania`
--

INSERT INTO `pytania` (`id`, `pytanie`, `odp a`, `odp b`, `odp c`, `odp d`, `prawidlowa odpowiedz`) VALUES
(1, 'Stosunek ladunku zgromadzonego na przewodniku do potencjalu tego przewodnika okresla jego', 'moc', 'rezystencje', 'indukcyjnosc', 'pojemnosc elektryczna', 'D'),
(2, 'Czynnym elementem elektronicznym jest', 'cewka.\r\n', 'rezystor.', 'tranzystor.', 'kondensator.', 'C'),
(3, 'Wskaż element, który dopasowuje poziom napięcia z sieci energetycznej przy użyciu transformatora \r\nprzenoszącego energię z jednego obwodu elektrycznego do drugiego z wykorzystaniem zjawiska indukcji \r\nmagnetycznej.', 'Rejestr szeregowy.\r\n', 'Rezonator kwarcowy.', 'Przerzutnik synchroniczny.\r\n', 'Zasilacz transformatorowy.', 'D'),
(4, 'Podczas instalacji systemu Windows, tuż po uruchomieniu instalatora w trybie graficznym, możliwe jest \r\nuruchomienie Wiersza poleceń (konsoli) za pomocą kombinacji przycisków', 'ALT + F4', 'CTRL + Z', 'SHIFT + F10', 'CTRL + SHIFT', 'C'),
(5, 'Po zainstalowaniu systemu Windows 10, aby skonfigurować połączenie internetowe z limitem danych, \r\nw ustawieniach sieci i Internetu należy ustawić połączenie', 'taryfowe.', 'przewodowe.', 'bezprzewodowe.', 'szerokopasmowe.\r\n', 'A'),
(6, 'Licencja dostępowa w systemie Windows Server, umożliwiająca użytkownikom stacji roboczych korzystanie \r\nz usług serwera to licencja\r\n', 'BOX', 'CAL', 'OEM', 'MOLP', 'B'),
(7, 'Cechą charakterystyczną topologii gwiazdy jest', 'małe zużycie kabla', 'centralne zarządzanie siecią.', 'trudna lokalizacja uszkodzeń.', 'blokada sieci w wyniku awarii terminala.\r\n', 'B'),
(8, 'Co można powiedzieć o budowie skrętki S/FTP?', 'Każda para przewodów jest foliowana i dodatkowo całość w ekranie z siatki.', 'Każda para przewodów jest w osobnym ekranie z folii, całość jest nieekranowana.', 'Każda para przewodów jest foliowana i dodatkowo całość w ekranie z folii i siatki.', 'Każda para przewodów jest w osobnym ekranie z folii i dodatkowo całość w ekranie z folii.', 'A'),
(9, 'Testowanie okablowania strukturalnego światłowodowego można wykonać za pomocą', 'sondy logicznej.', 'stacji lutowniczej.', 'odsysacza próżniowego.\r\n', 'reflektometru optycznego.\r\n', 'D'),
(10, 'Adresem rozgłoszeniowym w podsieci o adresie IPv4 192.168.0.0/20 jest', '192.168.255.255', '192.168.255.254', '192.168.15.255', '192.168.15.254\r\n', 'C'),
(11, 'W adresacji IPv6 adres ff00::/8 określa', 'adres nieokreślony.', 'pulę adresów testowej sieci 6bone.', 'adres wskazujący na lokalnego hosta.', 'pulę adresów używanych do komunikacji multicast.', 'D');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `pytania`
--
ALTER TABLE `pytania`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT dla zrzuconych tabel
--

--
-- AUTO_INCREMENT dla tabeli `pytania`
--
ALTER TABLE `pytania`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
